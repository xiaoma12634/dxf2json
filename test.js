const { DxfParser, toGeojson } = require('./')
const fs = require('fs')
const iconv = require('iconv-lite')

let name = 'sample2'
let dxfPath = `./sample/${name}.dxf`

let dxfContent = fs.readFileSync(dxfPath, 'binary')
dxfContent = iconv.decode(dxfContent, 'utf-8')
new DxfParser().parseContent(dxfContent)
    .then(res => {
        console.log(1111111)
        fs.writeFileSync(`${name}.json`, JSON.stringify(res, null, 4))
        let result = toGeojson(res)
        fs.writeFileSync(`${name}.geojson`, JSON.stringify(result, null, 4));
    })