# dxf2json

[![NPM version][npm-image]][npm-url]
[![NPM downloads][downloads-image]][downloads-url]
[![Build status][travis-image]][travis-url]

convert dxf file into json or geojson format

## Getting started

```shell
npm install dxf2json --save
```



```javascript
const fs = require('fs')
const { DxfParser, toGeojson } = require('dxf2json')

new DxfParser().init('./sample/file2.dxf' , 'out.json')
.then(result=>{
    return result 
})

const content = fs.readFileSync('./sample/file2.dxf', 'utf-8')
new DxfParser().parseContent(content)
.then(result=>{
    return toGeojson(result) 
})

```

references: 

* [dxftojson](https://www.npmjs.com/package/dxftojson)
