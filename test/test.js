const { DxfParser, toGeojson } = require('../');
const assert = require('chai').assert;
const fs = require('fs')

describe('Test suit', function () {
  it('should be ok', function (done) {
    let name='20210514-苏区至汕尾和汕尾至陆丰-2000-117'
    let dxfPath = `./sample/${name}.dxf`
    const dxfContent = fs.readFileSync(dxfPath, 'utf-8')
    new DxfParser().parseContent(dxfContent)
    .then(res => {
      console.log(1111111)
      fs.writeFileSync(`${name}.json`, JSON.stringify(res, null, 4))
      let result = toGeojson(res)
      fs.writeFileSync(`${name}.geojson`, JSON.stringify(result, null, 4));
	    // assert.containsAllKeys(res, ['HEADER', 'TABLES', 'BLOCKS', 'ENTITIES']);
			done();
    })
    .catch(err=>{
      console.log(222222222)
      console.log(err)
    })
  });
});