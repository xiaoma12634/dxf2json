const BaseParser = require('../common/base-parser').BaseParser;
const Promise = require('bluebird');

const LWPOLYLINE = require('./entity/lwpolyline').LWPOLYLINE;
const MLINE = require('./entity/mline').MLINE;
const DIMENSION = require('./entity/dimension').DIMENSION;
const WIPEOUT = require('./entity/wipeout').WIPEOUT;

class EntitiesParser extends BaseParser{
    constructor(data){
        super();
        this.data = data;
        this.result = [];
        this.entitySupport = {
            'ATTDEF' : { support : true, fn : new BaseParser() },
            'ARC' : { support : true, fn : new BaseParser() },
            'CIRCLE' : { support : true, fn : new BaseParser() },
            'ELLIPSE' : { support : true, fn : new BaseParser() },
            'INSERT' : { support : true, fn : new BaseParser() },
            'LINE' : { support : true, fn : new BaseParser() },
            'LWPOLYLINE' : { support : true, fn : new LWPOLYLINE() },
            'MTEXT' : { support : true, fn : new BaseParser() },
            'POINT' : { support : true, fn : new BaseParser() },

            'POLYLINE' : { support : true, fn : new BaseParser() },
            'VERTEX' : { support : false, fn : null },
            'SEQEND' : { support : false, fn : null },

            'SOLID' : { support : true, fn : new BaseParser() },
            'TEXT' : { support : true, fn : new BaseParser() },

            'DIMENSION' : { support : true, fn : new DIMENSION() },
            'HATCH' : { support : false, fn : null },
            '3DFACE' : { support : false, fn : null },
            '3DSOLID' : { support : false, fn : null },
            'ATTRIB' : { support : false, fn : null },
            'BODY' : { support : false, fn : null },
            'ACAD_PROXY_ENTITY' : { support : false, fn : null },
            'HELIX' : { support : false, fn : null },
            'IMAGE' : { support : false, fn : null },
            'LEADER' : { support : false, fn : null },
            'LIGHT' : { support : false, fn : null },
            'MESH' : { support : false, fn : null },
            'MLINE' : { support : true, fn : new MLINE() },
            'MLEADERSTYLE' : { support : false, fn : null },
            'MLEADER' : { support : false, fn : null },
            'OLEFRAME' : { support : false, fn : null },
            'OLE2FRAME' : { support : false, fn : null },
            'RAY' : { support : false, fn : null },
            'REGION' : { support : false, fn : null },
            'SECTION' : { support : false, fn : null },
            'SHAPE' : { support : false, fn : null },
            'SPLINE' : { support : false, fn : null },
            'SUN' : { support : false, fn : null },
            'SURFACE' : { support : false, fn : null },
            'TABLE' : { support : false, fn : null },
            'TOLERANCE' : { support : false, fn : null },
            'TRACE' : { support : false, fn : null },
            'UNDERLAY' : { support : false, fn : null },
            'VIEWPORT' : { support : false, fn : null },
            'WIPEOUT' : { support : true, fn : new WIPEOUT() },
            'XLINE' : { support : false, fn : null },
        };

        this.restoreFlag = false;
    }

    parse(){
        let entityIdx = -1;
        return Promise.each(this.data, (entities)=>{
            let entityType = entities.type;
            if( this.entitySupport.hasOwnProperty(entityType) ){
                if(this.entitySupport[entityType].support){
                    entityIdx++;
                    this.result[entityIdx] = {type : entities.type};
                } else {
                //   console.log('unsupport type', entityType);
                }
            } else {
              console.log('undefined type', entityType);
            }
            if(this.entitySupport.hasOwnProperty(entityType) && this.entitySupport[entityType].support){
                return Promise.each(entities.data, (cursor)=>{
                    return this.entitySupport[entityType].fn.parse(this.result[entityIdx],cursor);
                }).then(()=>{
                    return this.entitySupport[entityType].fn.reset();
                });
            } else {
                return false;
            }
        }).then(rtn=>{
            return this.result;
        });
    }
}

exports.EntitiesParser = EntitiesParser;
