const BaseParser = require('../../common/base-parser').BaseParser;
const Promise = require('bluebird');

class WIPEOUT extends BaseParser{
	constructor(){
		super();
		this.brightness = 50;
		this.contrast = 50;
		this.fade = 0;
	}

	parse(buffer, cursor){
		if(this.DxfSpec.Section.Entities.hasOwnProperty(cursor.code)){
			let spec = this.DxfSpec.Section.Entities[cursor.code];
			this.matchSpec(buffer, spec, cursor);
		} else if (this.DxfSpec.Entity.WIPEOUT.hasOwnProperty(cursor.code)) {
			let spec = this.DxfSpec.Entity.WIPEOUT[cursor.code];
			this.matchSpec(buffer, spec, cursor);
		} else {
			// console.log(cursor);
		}
	}

	reset(){
		this.brightness = 50;
		this.contrast = 50;
		this.fade = 0;
	}
}

exports.WIPEOUT = WIPEOUT;
